#!/usr/bin/env python

from distutils.core import setup

from os.path import abspath, dirname, join
execfile(join(dirname(abspath(__file__)), 'src', 'SocketIOLibrary', 'version.py'))

DESCRIPTION = """
Robot Framework keyword library wrapper around python's socketIO-client library.
"""[1:-1]


CLASSIFIERS = """
Development Status :: 3 - Alpha
License :: Public Domain
Operating System :: OS Independent
Programming Language :: Python
Topic :: Software Development :: Testing
"""[1:-1]

setup(name         = 'robotframework-socketio',
      version      = VERSION,
      description  = 'Robot Framework keyword library wrapper for socketio.',
      long_description = DESCRIPTION,
      author       = 'Olivier Leduc',
      author_email = 'pro@leduc.cc',
      url          = 'https://github.com/oleduc/robotframework-socketio',
      license      = 'Public Domain',
      keywords     = 'robotframework testing test automation socketio client',
      platforms    = 'any',
      classifiers  = CLASSIFIERS.splitlines(),
      package_dir  = {'' : 'src'},
      packages     = ['SocketIOLibrary'],
      package_data = {'SocketIOLibrary': ['tests/*.robot']},
      install_requires=[
          'robotframework',
          'socketIO-client'
      ],
)
