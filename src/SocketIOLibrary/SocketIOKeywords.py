import traceback
import robot

from Queue import Queue, Full, Empty
from robot.libraries.BuiltIn import BuiltIn
from robot.api import logger
from socketIO_client import SocketIO, LoggingNamespace


class SocketIOKeywords(object):
    _DEFAULT_TIMEOUT = 5

    def __init__(self):
        self.conns = {}

    def create_socketio(self, name, address, port, **kwargs):
        self.conns[name] = {
            "address": address,
            "port": int(port),
            "kwargs": kwargs,
            "socketIO": None
        }

    def create_socketio_namespace(self, conn_name, namespace):
        pass

    def connect_socketio(self, name):
        try:
            self.conns[name]["socketIO"] = SocketIO(
                self.conns[name]["address"],
                self.conns[name]["port"],
                wait_for_connection=False,
                **self.conns[name]["kwargs"]
            )

            if not self.conns[name]["socketIO"].connected:
                raise SocketIOError("Could not connect to server")
        except Exception as e:
            print traceback.format_exc()
            raise SocketIOError("Error while connecting to server")

    def disconnect_socketio(self, name):
        self.conns[name]["socketIO"].disconnect()

    def emit(self, name, action, data=None):
        self.conns[name]["socketIO"].emit_binary(action, data)

    def expect_socketio_event(self, name, event_name, timeout=1):
        resp = SocketEventResponse()

        def notify_of_event(data):
            resp.fulfill()
            resp.set_data(data)

        self.conns[name]["socketIO"].on(event_name, notify_of_event)
        self.conns[name]["socketIO"].wait(int(timeout))

        if resp.is_fulfilled():
            return resp.data
        else:
            raise SocketOperationTimeout("Did not get \"{event_name}\" event in time".format(event_name=event_name))

    def supervise_server_initiated_exchange(self, name, start_event, handler_keyword, params):
        response = self.expect_socketio_event(name, start_event)
        reply = BuiltIn().run_keyword(handler_keyword, start_event, response, params)

        while reply.expected_reply is not False:
            self.emit(name, reply.action_to_send, reply.data_to_send)
            response = self.expect_socketio_event(name, reply.expected_reply)
            reply = BuiltIn().run_keyword(handler_keyword, reply.expected_reply, response)

            if not isinstance(reply, EventReply):
                raise EventReplyInvalid()

    def register_error_event(self, conn, event_name):
        pass

    def socketio_events_should_not_happen(self, conn, event_names, within=_DEFAULT_TIMEOUT):
        pass

    def socketio_event_should_happen_x_times(self, conn, event_names, times, y, within=_DEFAULT_TIMEOUT):
        pass

    def wait_for_socketio(self, conn, time):
        pass


class EventReply(object):
    def __init__(self, expected_reply_event, action_to_send, data_to_send):
        self.sent = False
        self.action_to_send = action_to_send
        self.data_to_send = data_to_send
        self.expected_reply = expected_reply_event


class EventReplyInvalid(ValueError):
    pass


class SocketEventResponse(object):
    def __init__(self):
        self.fulfilled = False
        self.data = None

    def fulfill(self):
        self.fulfilled = True

    def is_fulfilled(self):
        return self.fulfilled

    def set_data(self, data):
        self.data = data


class SocketIOError(Exception):
    pass


class SocketOperationTimeout(SocketIOError):
    pass
