from SocketIOKeywords import SocketIOKeywords
from version import VERSION

_version_ = VERSION


class SocketIOLibrary(SocketIOKeywords):
    """ SocketIOLibrary is a SocketIO client keyword library that uses
    the socketIO-client module from Roy Hyunjin Han
    https://github.com/invisibleroads/socketIO-client


        Examples:
        | # Setup |
        | Create SocketIO | file-server | localhost | 3001 |
        | # Emit |
        | Emit | file-server | upload | ${data} |
        | # Event |
        | ${response} | Expect socketIO event | file-server | uploaded |
        | Expect events | file-server | ${events}
        | # Events |
        | ${responses} | Expect socketIO event | file-server | uploaded |
        | Expect events | file-server | ${events}
    """
    ROBOT_LIBRARY_SCOPE = 'GLOBAL'
